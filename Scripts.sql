


CREATE DATABASE prueba;


CREATE TABLE cities
   (
      Id int IDENTITY(1,1) PRIMARY KEY,
      Name varchar(255),
	  State char(1)
   );

insert into cities (Name, State) values ('Bogota', 'A');
insert into cities (Name, State) values ('Medellin', 'A');
insert into cities (Name, State) values ('Cali', 'A');

CREATE TABLE hotels
   (
      Id int IDENTITY(1,1) PRIMARY KEY,
      Name varchar(255),
      Id_city int,
	  State char(1),

	  CONSTRAINT FK_Hotels_Cities FOREIGN KEY (Id_city) REFERENCES cities(Id)
   );

insert into hotels (Name, Id_city, State) values ('Hotel bn Usaquen', 1, 'A');
insert into hotels (Name, Id_city, State) values ('Hotel Dann Carlton', 2, 'A');



CREATE TABLE rooms
   (
      Id int IDENTITY(1,1) PRIMARY KEY,
      Name varchar(255),
      Id_hotel int,
	  Price decimal,
	  Capacity int,
	  State char(1),

	  CONSTRAINT FK_Rooms_Hotels FOREIGN KEY (Id_hotel) REFERENCES hotels(Id)
   );

insert into rooms (Name, Id_hotel, Price, Capacity, State) values ('101', 1, 150000, 2, 'A');
insert into rooms (Name, Id_hotel, Price, Capacity, State) values ('102', 1, 165000, 3, 'A');
insert into rooms (Name, Id_hotel, Price, Capacity, State) values ('103', 1, 150000, 2, 'A');
insert into rooms (Name, Id_hotel, Price, Capacity, State) values ('104', 1, 165000, 3, 'A');

insert into rooms (Name, Id_hotel, Price, Capacity, State) values ('201', 2, 350000, 2, 'A');
insert into rooms (Name, Id_hotel, Price, Capacity, State) values ('202', 2, 365000, 4, 'A');
insert into rooms (Name, Id_hotel, Price, Capacity, State) values ('203', 2, 350000, 2, 'A');
insert into rooms (Name, Id_hotel, Price, Capacity, State) values ('204', 2, 365000, 4, 'A');



   CREATE TABLE Identification_types
   (
      Id int IDENTITY(1,1) PRIMARY KEY,
      Name varchar(255),
	  State char(1),
   );

insert into Identification_types (Name, State) values ('Cedula de ciudadania', 'A');
insert into Identification_types (Name, State) values ('Tarjeta de identidad', 'A');


CREATE TABLE guests
   (
      Id int IDENTITY(1,1) PRIMARY KEY,
      Name_lastname varchar(255),
	  BirthDate datetime,
	  Gender char(1),
	  Id_type int,
	  Number_Id int,
	  Email_address varchar(150),
	  Phone_Number varchar(150),
	  ContactEmergency_Name_lastname varchar(255),
	  ContactEmergency_Phone_Number varchar(150),
	  State char(1),

	  CONSTRAINT FK_guest_IdTypes FOREIGN KEY (Id_type) REFERENCES Identification_types(Id)
   );

   insert into guests ( Name_lastname, BirthDate, Gender, Id_type, Number_Id, Email_address,
		Phone_Number, ContactEmergency_Name_lastname, ContactEmergency_Phone_Number, State) 
		values ('Ross Jr Tique Montoya', '1994-09-24', 'M', 1, 1110551491, 'Rossjunior9436@gmail.com',
		'3116754088', 'Kelly Lozada', '3116754088', 'A');

   CREATE TABLE reservations
   (
      Id int IDENTITY(1,1) PRIMARY KEY,
      Id_hotel int,
	  Id_room int,
	  Checkin datetime,
	  Checkout datetime,
	  Total_price decimal,
	  total_guest int,
	  State char(1),
	  CONSTRAINT FK_reservation_hotels FOREIGN KEY (Id_hotel) REFERENCES Hotels(Id),
	  CONSTRAINT FK_reservation_rooms FOREIGN KEY (Id_room) REFERENCES Rooms(Id)
   );

     insert into reservations ( Id_hotel, Id_room, Checkin, Checkout, Total_price, total_guest, State) 
		values (1, 1, '2024-02-18', '2024-02-20', 150000, 1, 'A');

   CREATE TABLE guest_reservations
   (
      Id int IDENTITY(1,1) PRIMARY KEY,
      Id_reservation int,
	  Id_guest int,
	  CONSTRAINT FK_guestReservation_reservation FOREIGN KEY (Id_reservation) REFERENCES reservations(Id),
	  CONSTRAINT FK_guestReservation_guest FOREIGN KEY (Id_guest) REFERENCES guests(Id)
   );

     insert into guest_reservations ( Id_reservation, Id_guest) 
		values (1, 1);
