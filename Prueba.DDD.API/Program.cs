using Microsoft.AspNetCore.Localization;
using Microsoft.EntityFrameworkCore;
using Microsoft.OpenApi.Models;
using Prueba.DDD.API.ApplicationServices;
using Prueba.DDD.API.Queries;
using Prueba.DDD.Domain.Repositories;
using Prueba.DDD.Infraestructure;
using Prueba.DDD.Infraestructure.Repository;
using System.Globalization;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.




builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddDbContext<DataBaseContext>(
    options => options.UseSqlServer(builder.Configuration.GetConnectionString("prueba")));
builder.Services.AddScoped<IHotelRepository, HotelRepository>();
builder.Services.AddScoped<HotelQueries>();
builder.Services.AddScoped<HotelServices>();

builder.Services.AddScoped<IRoomRepository, RoomRepository>();
builder.Services.AddScoped<RoomQueries>();
builder.Services.AddScoped<RoomServices>();

builder.Services.AddScoped<IidentificationTypeRepository, IdentificationRepository>();
builder.Services.AddScoped<IdentificationTypeQueries>();
builder.Services.AddScoped<IdentificationTypeServices>();


builder.Services.AddScoped<IReservationRepository, ReservationRepository>();
builder.Services.AddScoped<ReservationQueries>();
builder.Services.AddScoped<ReservationServices>();

builder.Services.AddSwaggerGen(c =>
{
    c.SwaggerDoc("v1", new OpenApiInfo { Title = "NombreDeTuProyecto", Version = "v1" });
});

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI(
    );
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
