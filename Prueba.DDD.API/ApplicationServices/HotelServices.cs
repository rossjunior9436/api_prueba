﻿using Prueba.DDD.API.Commands.HotelCommands;
using Prueba.DDD.API.Queries;
using Prueba.DDD.Domain.Entities;
using Prueba.DDD.Domain.Repositories;
using Prueba.DDD.Domain.ValueObjects;


namespace Prueba.DDD.API.ApplicationServices
{
    public class HotelServices
    {
        private IHotelRepository hotelRepository;
        private HotelQueries hotelQueries;
        public HotelServices(IHotelRepository hotelRepository,
            HotelQueries hotelQueries) { 
            this.hotelRepository = hotelRepository;
            this.hotelQueries = hotelQueries;
        }
        public async Task HandleCommand(CreateHotelCommand createHotel) {
            var hotel = new Hotel();
            hotel.SetName(Name.Create(createHotel.name));
            hotel.SetId_city(IdObject.Create(createHotel.cityId));
            hotel.SetState(State.Create(createHotel.state));
            hotel.Id = 0;

            await hotelRepository.AddHotel( hotel );
        }
        public async Task<Hotel> GetHotel(int id) { 
            return await hotelQueries.GetHotelIdAsync(id);
        }


        public async Task UpdateHandleCommand(UpdateHotelCommand updateHotel)
        {
            var hotel = new Hotel();
            hotel.SetName(Name.Create(updateHotel.name));
            hotel.SetId_city(IdObject.Create(updateHotel.cityId));
            hotel.SetState(State.Create(updateHotel.state));
            hotel.Id = updateHotel.id;

            await hotelRepository.UpdateHotel(hotel);
        }
    }
}
