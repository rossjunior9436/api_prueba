﻿using Prueba.DDD.API.Commands.HotelCommands;
using Prueba.DDD.API.Queries;
using Prueba.DDD.Domain.Entities;
using Prueba.DDD.Domain.Repositories;
using Prueba.DDD.Domain.ValueObjects;

namespace Prueba.DDD.API.ApplicationServices
{

    public class IdentificationTypeServices
    {
        private IidentificationTypeRepository _identificationTypeRepository;
        private IdentificationTypeQueries _identificationTypeQueries;
        public IdentificationTypeServices(IidentificationTypeRepository identificationTypeRepository,
            IdentificationTypeQueries identificationTypeQueries)
        {
            this._identificationTypeRepository = identificationTypeRepository;
            this._identificationTypeQueries = identificationTypeQueries;
        }

        public async Task<Identification_type> GetIdentificationType(int id)
        {
            return await _identificationTypeQueries.GetIdentificationTypeIdAsync(id);
        }

    }
}
