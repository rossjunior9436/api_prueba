﻿using Prueba.DDD.API.Commands.HotelCommands;
using Prueba.DDD.API.Commands.RoomCommands;
using Prueba.DDD.API.Queries;
using Prueba.DDD.Domain.Entities;
using Prueba.DDD.Domain.Repositories;
using Prueba.DDD.Domain.ValueObjects;

namespace Prueba.DDD.API.ApplicationServices
{

    public class RoomServices
    {
        private IRoomRepository _roomRepository;
        private RoomQueries _roomQueries;
        public RoomServices(IRoomRepository roomRepository,
            RoomQueries roomQueries)
        {
            this._roomRepository = roomRepository;
            this._roomQueries = roomQueries;
        }
        public async Task HandleCommand(CreateRoomCommand createRoom)
        {
            var room = new Room();
            room.SetName(Name.Create(createRoom.name));
            room.SetId_hotel(IdObject.Create(createRoom.hotelId));
            room.SetPrice(Price.Create(createRoom.price));
            room.SetCapacity(Capacity.Create(createRoom.capacity));
            room.SetState(State.Create(createRoom.state));
            room.Id = 0;

            await _roomRepository.AddRoom(room);
        }
        public async Task<Room> GetRoom(int id)
        {
            return await _roomQueries.GetRoomIdAsync(id);
        }

        public async Task<List<Room>> GetAvailableRoom(DateTime checking, DateTime CheckOut, int totalGuest, int id_city)
        {
            return await _roomQueries.GetAvailableRoomAsync(checking, CheckOut, totalGuest, id_city);
        }

        public async Task UpdateHandleCommand(UpdateRoomCommand updateRoom)
        {

            var room = new Room();
            room.SetName(Name.Create(updateRoom.name));
            room.SetId_hotel(IdObject.Create(updateRoom.hotelId));
            room.SetPrice(Price.Create(updateRoom.price));
            room.SetCapacity(Capacity.Create(updateRoom.capacity));
            room.SetState(State.Create(updateRoom.state));
            room.Id = updateRoom.id;

            await _roomRepository.UpdateRoom(room);
        }
    }
}
