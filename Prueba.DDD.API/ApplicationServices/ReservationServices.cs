﻿using Prueba.DDD.API.Commands.HotelCommands;
using Prueba.DDD.API.Commands.ReservationCommands;
using Prueba.DDD.API.Queries;
using Prueba.DDD.Domain.Entities;
using Prueba.DDD.Domain.Repositories;
using Prueba.DDD.Domain.ValueObjects;
using System.ComponentModel.DataAnnotations;

namespace Prueba.DDD.API.ApplicationServices
{
    public class ReservationServices
    {
        private IReservationRepository _reservationRepository;
        private ReservationQueries _reservationQueries;

        private RoomQueries _roomQueries;
        public ReservationServices(IReservationRepository reservationRepository,
            ReservationQueries reservationQueries, RoomQueries _roomQueries)
        {
            this._reservationRepository = reservationRepository;
            this._reservationQueries = reservationQueries;
            this._roomQueries = _roomQueries;
        }
        public async Task HandleCommand(CreateReservationCommand createReservation)
        {
            var reservation = new Reservation();
            reservation.SetId_hotel(IdObject.Create(createReservation.reservation.idHotel));
            reservation.SetId_room(IdObject.Create(createReservation.reservation.Id_room));
            reservation.SetCheckIn(CheckDate.Create(createReservation.reservation.CheckIn));
            reservation.SetCheckOut(CheckDate.Create(createReservation.reservation.CheckOut));
            reservation.SetState(State.Create('A'));
            reservation.SetTotal_guest(Capacity.Create(createReservation.reservation.Guests.Count()));
            var room = await _roomQueries.GetRoomIdAsync(createReservation.reservation.Id_room);
            reservation.SetTotal_price(Price.Create(room.Price.Value));
            reservation.Id = 0;

            List<Guest> lst_guest  = new List<Guest>();
            
            foreach (var itemGuest in createReservation.reservation.Guests)
            {
                Guest guest = new Guest();
                guest.SetName(Name.Create(itemGuest.NameAndLastName));
                guest.SetBirthDate(BirthDate.Create(itemGuest.BirthDate));
                guest.SetGender(Gender.Create(itemGuest.Gender));
                guest.SetId_Type(IdObject.Create(itemGuest.Id_Type));
                guest.SetNumberId(NumberId.Create(itemGuest.NumberId));
                guest.SetEmail(Email.Create(itemGuest.EmailAddress));
                guest.SetPhoneNumber(PhoneNumber.Create(itemGuest.PhoneNumber));
                guest.SetContactEmergencyNameAndLastName(Name.Create(itemGuest.ContactEmergencyNameAndLastName));
                guest.SetContactEmergencyPhoneNumber(PhoneNumber.Create(itemGuest.ContactEmergencyPhoneNumber));
                guest.SetState(State.Create('A'));
                guest.Id = 0;
                lst_guest.Add(guest);
            }


        await _reservationRepository.AddReservation(reservation, lst_guest);
        }

        public async Task<List<Reservation>> GetReservation(DateTime initialDate, DateTime finalDate)
        {
            return await _reservationQueries.GetReservationRangeDateAsync(initialDate, finalDate);
        }


    }
}
