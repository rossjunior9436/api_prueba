﻿using Prueba.DDD.Domain.Entities;
using Prueba.DDD.Domain.Repositories;

namespace Prueba.DDD.API.Queries
{

    public class RoomQueries
    {
        private IRoomRepository _roomRepository;
        public RoomQueries(IRoomRepository roomRepository)
        {
            this._roomRepository = roomRepository;
        }
        public async Task<Room> GetRoomIdAsync(int id)
        {
            var response = await _roomRepository.GetRoomById(id);

            return response;
        }

        public async Task<List<Room>> GetAvailableRoomAsync(DateTime checking, DateTime CheckOut, int totalGuest, int id_city)
        {
            var response = await _roomRepository.GetAvailableRoom(checking, CheckOut, totalGuest, id_city);

            return response;
        }

    }
}
