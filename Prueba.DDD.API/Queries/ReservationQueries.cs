﻿using Prueba.DDD.Domain.Entities;
using Prueba.DDD.Domain.Repositories;

namespace Prueba.DDD.API.Queries
{


    public class ReservationQueries
    {
        private IReservationRepository _reservationRepository;
        public ReservationQueries(IReservationRepository reservationRepository)
        {
            this._reservationRepository = reservationRepository;
        }
        /*public async Task<Hotel> GetHotelIdAsync(Guid id) {
            var response = await _hotelRepository.GetHotelById(
                HotelId.create(id));

            return response;
        }*/

        public async Task<List<Reservation>> GetReservationRangeDateAsync(DateTime initialDate, DateTime finalDate)
        {
            var response = await _reservationRepository.GetReservationByRangeDate(initialDate, finalDate);

            return response;
        }

    }
}
