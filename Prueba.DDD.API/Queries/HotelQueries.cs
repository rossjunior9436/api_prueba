﻿using Prueba.DDD.Domain.Entities;
using Prueba.DDD.Domain.Repositories;


namespace Prueba.DDD.API.Queries
{
    public class HotelQueries
    {
        private IHotelRepository _hotelRepository;
        public HotelQueries(IHotelRepository _hotelRepository) { 
            this._hotelRepository = _hotelRepository;
        }
        /*public async Task<Hotel> GetHotelIdAsync(Guid id) {
            var response = await _hotelRepository.GetHotelById(
                HotelId.create(id));

            return response;
        }*/

        public async Task<Hotel> GetHotelIdAsync(int id)
        {
            var response = await _hotelRepository.GetHotelById(id);

            return response;
        }

    }
}
