﻿using Prueba.DDD.Domain.Entities;
using Prueba.DDD.Domain.Repositories;

namespace Prueba.DDD.API.Queries
{


    public class IdentificationTypeQueries
    {
        private IidentificationTypeRepository _IidentificationTypeRepository;
        public IdentificationTypeQueries(IidentificationTypeRepository identificationTypeRepository)
        {
            this._IidentificationTypeRepository = identificationTypeRepository;
        }
        /*public async Task<Hotel> GetHotelIdAsync(Guid id) {
            var response = await _hotelRepository.GetHotelById(
                HotelId.create(id));

            return response;
        }*/

        public async Task<Identification_type> GetIdentificationTypeIdAsync(int id)
        {
            var response = await _IidentificationTypeRepository.GetIdentificationTypeById(id);

            return response;
        }

    }
}
