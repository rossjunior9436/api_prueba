﻿namespace Prueba.DDD.API.Commands.HotelCommands
{
    public record CreateHotelCommand(string name, int cityId, char state)
    {
    }
}
