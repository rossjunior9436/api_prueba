﻿namespace Prueba.DDD.API.Commands.HotelCommands
{

    public record UpdateHotelCommand(int id, string name, int cityId, char state)
    {
    }
}
