﻿using Prueba.DDD.Domain.Entities;
using Prueba.DDD.Domain.ValueObjects;

namespace Prueba.DDD.API.Commands.ReservationCommands
{
    public class DetailsReservation() {
        public int idHotel { get; set; }
        public int Id_room { get; set; }
        public DateTime CheckIn { get; set; }
        public DateTime CheckOut { get; set; }

        public List<DetailsGuest> Guests { get; set; }

    }

    public class DetailsGuest()
    {
        public string NameAndLastName { get; set; }
        public DateTime BirthDate { get; set; }
        public char Gender { get; set; }
        public int Id_Type { get; set; }
        public int NumberId { get; set; }
        public string EmailAddress { get; set; }
        public string PhoneNumber { get; set; }

        /* Datos del contacto de emergencia */
        public string ContactEmergencyNameAndLastName { get; set; }
        public string ContactEmergencyPhoneNumber { get; set; }

    }


    public record CreateReservationCommand(DetailsReservation reservation)
    {
    }
}
