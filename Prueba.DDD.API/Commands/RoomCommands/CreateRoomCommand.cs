﻿namespace Prueba.DDD.API.Commands.RoomCommands
{
    public record CreateRoomCommand(string name, int hotelId, decimal price, int capacity, char state)
    {
    }
}
