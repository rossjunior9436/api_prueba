﻿namespace Prueba.DDD.API.Commands.RoomCommands
{
    public record UpdateRoomCommand(int id, string name, int hotelId, decimal price, int capacity, char state)
    {
    }
}
