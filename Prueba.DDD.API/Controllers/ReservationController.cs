﻿using Microsoft.AspNetCore.Mvc;
using Prueba.DDD.API.ApplicationServices;
using Prueba.DDD.API.Commands.HotelCommands;
using Prueba.DDD.API.Commands.ReservationCommands;
using Swashbuckle.AspNetCore.Annotations;

namespace Prueba.DDD.API.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class ReservationController : ControllerBase
    {
        private readonly ReservationServices _reservationServices;
        public ReservationController(ReservationServices reservationServices)
        {
            this._reservationServices = reservationServices;
        }
        [HttpPost]
        [SwaggerOperation(Summary = "Crear Reservaciones", Description = "Permite que desde el front se envíe toda la información necesaria para la creación de una reserva")]
        public async Task<IActionResult> AddReservation(CreateReservationCommand createReservationCommand)
        {
            await _reservationServices.HandleCommand(createReservationCommand);
            return Ok(createReservationCommand);
        }

        [HttpGet("ListReservations")]
        [SwaggerOperation(Summary = "Obtener Reservaciones por un rango de fechas", Description = "Permite que desde el front se envíe un rango de fechas para asi poder realizar el filtro necesario " +
            "y observar las reservas de cada habitación")]
        public async Task<IActionResult> GetReservations(DateTime initialDate, DateTime finalDate)
        {
            var response = await _reservationServices.GetReservation(initialDate, finalDate);
            return Ok(response);
        }

    }
}
