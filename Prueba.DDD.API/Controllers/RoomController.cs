﻿using Microsoft.AspNetCore.Mvc;
using Prueba.DDD.API.ApplicationServices;
using Prueba.DDD.API.Commands.HotelCommands;
using Prueba.DDD.API.Commands.RoomCommands;
using Swashbuckle.AspNetCore.Annotations;

namespace Prueba.DDD.API.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class RoomController : ControllerBase
    {
        private readonly RoomServices _roomServices;
        public RoomController(RoomServices roomServices)
        {
            this._roomServices = roomServices;
        }
        [HttpPost]
        [SwaggerOperation(Summary = "Crear habitaciones", Description = "Permite crear habitaciones enviando desde el front los datos necesarios")]
        public async Task<IActionResult> AddRoom(CreateRoomCommand createRoomCommand)
        {
            await _roomServices.HandleCommand(createRoomCommand);
            return Ok(createRoomCommand);
        }

        [HttpGet("{id}")]
        [SwaggerOperation(Summary = "Obtener habitacion por id", Description = "Permite obtener la información de una habitación enviando su Id")]
        public async Task<IActionResult> GetRoom(int id)
        {
            var response = await _roomServices.GetRoom(id);
            return Ok(response);
        }

        [HttpGet("Available")]
        [SwaggerOperation(Summary = "Obtener las habitaciones disponibles por un rango de fecha", Description = "Permite obtener la información de las habitaciones disponibles para reservas " +
            " en una ciudad especifica.")]
        public async Task<IActionResult> GetAvailableRoom(DateTime checking, DateTime CheckOut, int totalGuest, int id_city)
        {
            var response = await _roomServices.GetAvailableRoom(checking, CheckOut, totalGuest, id_city);
            return Ok(response);
        }

        [HttpPut]
        [SwaggerOperation(Summary = "Actualizar habitacion por id", Description = "Permite actualizar todos los campos de la habitación enviando el ID a modificar" +
            " se deja así, para ser reutilizado en diversos casos y no solo para cambiar el estado")]
        public async Task<IActionResult> UpdateRoom(UpdateRoomCommand updateRoomCommand)
        {
            await _roomServices.UpdateHandleCommand(updateRoomCommand);
            return Ok(updateRoomCommand);
        }

    }
}
