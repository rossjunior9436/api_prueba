﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Prueba.DDD.API.ApplicationServices;
using Prueba.DDD.API.Commands.HotelCommands;
using Swashbuckle.AspNetCore.Annotations;
using System.ComponentModel;

namespace Prueba.DDD.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class HotelController : ControllerBase
    {
        private readonly HotelServices _hotelServices;
        public HotelController(HotelServices pruebaServices) {
            this._hotelServices = pruebaServices;
        }
        [HttpPost]
        [SwaggerOperation(Summary = "Crear Hoteles", Description = "Permite la creación de hoteles")]
        public async Task<IActionResult> AddHotel(CreateHotelCommand createHotelCommand) {
            await _hotelServices.HandleCommand(createHotelCommand);
            return Ok(createHotelCommand);
        }

        [HttpGet("{id}")]
        [SwaggerOperation(Summary = "Obtener el Hotel por id", Description = "Permite que desde el front se consuma el endpoint para asi buscar un hotel especifico")]
        public async Task<IActionResult> GetHotel(int id) { 
            var response = await _hotelServices.GetHotel(id);
            return Ok(response);
        }

        [HttpPut]
        [SwaggerOperation(Summary = "Actualizar el Hotel por id", Description = "Permite que desde el front se consuma el endpoint para actualizar el hotel, todos los campos enviados si son di" +
            " diferentes se modificara, se deja asi para reutilizarlo desde el front para no solo deshabilitar el hotel   ")]
        public async Task<IActionResult> UpdateHotel(UpdateHotelCommand updateHotelCommand)
        {
            await _hotelServices.UpdateHandleCommand(updateHotelCommand);
            return Ok(updateHotelCommand);
        }

    }
}
