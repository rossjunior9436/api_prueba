﻿using Microsoft.AspNetCore.Mvc;
using Prueba.DDD.API.ApplicationServices;
using Prueba.DDD.API.Commands.HotelCommands;
using Swashbuckle.AspNetCore.Annotations;

namespace Prueba.DDD.API.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class IdentificationTypeController : ControllerBase
    {
        private readonly IdentificationTypeServices _identificationTypeServices;
        public IdentificationTypeController(IdentificationTypeServices identificationTypeServices)
        {
            this._identificationTypeServices = identificationTypeServices;
        }
       
        [HttpGet("{id}")]
        [SwaggerOperation(Summary = "Obtener los tipos de identificación", Description = "Permite que desde el front se consuma el endpoint, para listar los tipos de identifiación")]
        public async Task<IActionResult> GetIdentificationType(int id)
        {
            var response = await _identificationTypeServices.GetIdentificationType(id);
            return Ok(response);
        }


    }
}
