﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prueba.DDD.Domain.ValueObjects
{
    public record Name
    {
        public string Value { get; init; }
        internal Name(string value)
        {
            Value = value;
        }

        public static Name Create(string value)
        {
            validate(value);
            return new Name(value);
        }

        private static void validate(string value)
        {
            if (value == null)
            {
                throw new ArgumentNullException("el nombre no puede ser nulo");
            }
        }
    }
}
