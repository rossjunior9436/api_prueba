﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prueba.DDD.Domain.ValueObjects
{

    public record NumberId
    {
        public int Value { get; init; }
        internal NumberId(int value_)
        {
            Value = value_;
        }
        public static NumberId Create(int value)
        {
            validate(value);
            return new NumberId(value);
        }

        private static void validate(int value)
        {
            if (value <= 0)
            {
                throw new ArgumentException("el número de identificación debe ser mayor a 0");
            }
        }

        private NumberId() { }

    }
}
