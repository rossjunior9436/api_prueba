﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prueba.DDD.Domain.ValueObjects
{

    public record Gender
    {
        public char Value { get; init; }
        internal Gender(char value)
        {
            Value = value;
        }

        public static Gender Create(char value)
        {
            validate(value);
            return new Gender(value);
        }

        private static void validate(char value)
        {
            if (value == null)
            {
                throw new ArgumentNullException("el nombre no puede ser nulo");
            }
        }
    }
}
