﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prueba.DDD.Domain.ValueObjects
{


    public record Identifier
    {
        public Guid value { get; init; }
        internal Identifier(Guid value_)
        {
            value = value_;
        }
        public static Identifier Create(Guid value)
        {
            return new Identifier(value);
        }
        public static implicit operator Guid(Identifier identifier)
        {
            return identifier.value;
        }
    }
}
