﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prueba.DDD.Domain.ValueObjects
{
    public record Email
    {
        public string Value { get; init; }
        internal Email(string value)
        {
            Value = value;
        }

        public static Email Create(string value)
        {
            validate(value);
            return new Email(value);
        }

        private static void validate(string value)
        {
            if (value == null)
            {
                throw new ArgumentNullException("el correo no puede ser nulo");
            }
        }
    }
}
