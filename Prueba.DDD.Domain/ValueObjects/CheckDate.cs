﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prueba.DDD.Domain.ValueObjects
{
    public record CheckDate
    {
        public DateTime Value { get; init; }
        internal CheckDate(DateTime value_)
        {
            Value = value_;
        }
        public static CheckDate Create(DateTime value)
        {
            //validate(value);
            return new CheckDate(value);
        }

        /*private static void validate(DateTime value)
        {
            if (value <= 0)
            {
                throw new ArgumentException("el id debe ser mayor a 0");
            }
        }*/

        private CheckDate() { }

    }
}
