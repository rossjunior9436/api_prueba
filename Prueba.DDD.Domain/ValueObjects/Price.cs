﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prueba.DDD.Domain.ValueObjects
{

    public record Price
    {
        public decimal Value { get; init; }
        internal Price(decimal value_)
        {
            Value = value_;
        }
        public static Price Create(decimal value)
        {
            validate(value);
            return new Price(value);
        }

        private static void validate(decimal value)
        {
            if (value <= 0)
            {
                throw new ArgumentException("el precio debe ser mayor a 0");
            }
        }

        private Price() { }

    }
}
