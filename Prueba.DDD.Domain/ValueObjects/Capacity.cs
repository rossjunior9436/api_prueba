﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prueba.DDD.Domain.ValueObjects
{

    public record Capacity
    {
        public int Value { get; init; }
        internal Capacity(int value_)
        {
            Value = value_;
        }
        public static Capacity Create(int value)
        {
            validate(value);
            return new Capacity(value);
        }

        private static void validate(int value)
        {
            if (value <= 0)
            {
                throw new ArgumentException("el id debe ser mayor a 0");
            }
        }

        private Capacity() { }

    }
}
