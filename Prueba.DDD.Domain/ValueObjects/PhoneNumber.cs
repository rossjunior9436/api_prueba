﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prueba.DDD.Domain.ValueObjects
{

    public record PhoneNumber
    {
        public string Value { get; init; }
        internal PhoneNumber(string value_)
        {
            Value = value_;
        }
        public static PhoneNumber Create(string value)
        {
            return new PhoneNumber(value);
        }
        private PhoneNumber() { }

    }
}
