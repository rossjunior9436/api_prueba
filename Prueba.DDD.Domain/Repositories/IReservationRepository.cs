﻿using Prueba.DDD.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prueba.DDD.Domain.Repositories
{
    public interface IReservationRepository
    {
        Task<List<Reservation>> GetReservationByRangeDate(DateTime initialDate, DateTime finalDate);
        Task AddReservation(Reservation reservation, List<Guest> guest);
    }
}
