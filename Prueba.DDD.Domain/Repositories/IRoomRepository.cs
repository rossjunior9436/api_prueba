﻿using Prueba.DDD.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prueba.DDD.Domain.Repositories
{

    public interface IRoomRepository
    {
        Task<Room> GetRoomById(int id);
        Task<List<Room>> GetAvailableRoom(DateTime checking, DateTime CheckOut, int totalGuest, int id_city);
        Task AddRoom(Room hotel);
        Task UpdateRoom(Room hotel);
    }
}
