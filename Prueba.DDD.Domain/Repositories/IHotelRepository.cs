﻿using Prueba.DDD.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prueba.DDD.Domain.Repositories
{
    public interface IHotelRepository
    {
        //Task<Hotel> GetHotelById(HotelId id);
        Task<Hotel> GetHotelById(int id);
        Task AddHotel(Hotel hotel);

        Task UpdateHotel(Hotel hotel);
    }
}
