﻿using Prueba.DDD.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prueba.DDD.Domain.Repositories
{

    public interface IidentificationTypeRepository
    {
        Task<Identification_type> GetIdentificationTypeById(int id);

    }
}
