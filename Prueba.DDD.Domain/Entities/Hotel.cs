﻿using Prueba.DDD.Domain.ValueObjects;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prueba.DDD.Domain.Entities
{
    public class Hotel
    {
        [Key]
        public int Id { get; set; }
        public Name Name { get;  set; }
        public IdObject Id_city { get; set; }
        public virtual City City { get; set; }

        [StringLength(1)]
        public State State { get;  set; }
        public void SetName(Name name) { 
            this.Name = name;
        }
        public void SetId_city(IdObject id_city)
        {
            this.Id_city = id_city;
        }
        public void SetState(State state)
        {
            this.State = state;
        }


    }
}

