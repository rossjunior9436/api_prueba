﻿using Prueba.DDD.Domain.ValueObjects;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prueba.DDD.Domain.Entities
{

    public class Guest
    {
        [Key]
        public int Id { get; set; }
        public Name NameAndLastName { get; set; }
        public BirthDate BirthDate { get; set; }
        public Gender Gender { get; set; }
        public IdObject Id_Type { get; set; }
        public virtual Identification_type Identification_Type { get; set; }
        public NumberId NumberId { get; set; }
        public Email EmailAddress { get; set; }
        public PhoneNumber PhoneNumber { get; set; }

        /* Datos del contacto de emergencia */
        public Name ContactEmergencyNameAndLastName { get; set; }
        public PhoneNumber ContactEmergencyPhoneNumber { get; set; }
        /***********************************/

        [StringLength(1)]
        public State State { get; set; }
        public void SetName(Name name)
        {
            this.NameAndLastName = name;
        }
        public void SetBirthDate(BirthDate birthDate)
        {
            this.BirthDate = birthDate;
        }
        public void SetGender(Gender gender)
        {
            this.Gender = gender;
        }
        public void SetId_Type(IdObject id_Type)
        {
            this.Id_Type = id_Type;
        }
        public void SetNumberId(NumberId numberId)
        {
            this.NumberId = numberId;
        }
        public void SetEmail(Email email)
        {
            this.EmailAddress = email;
        }
        public void SetPhoneNumber(PhoneNumber phoneNumber)
        {
            this.PhoneNumber = phoneNumber;
        }

        public void SetContactEmergencyNameAndLastName(Name contactEmergencyNameAndLastName)
        {
            this.ContactEmergencyNameAndLastName = contactEmergencyNameAndLastName;
        }
        public void SetContactEmergencyPhoneNumber(PhoneNumber contactEmergencyPhoneNumber)
        {
            this.ContactEmergencyPhoneNumber = contactEmergencyPhoneNumber;
        }

        public void SetState(State state)
        {
            this.State = state;
        }


    }
}
