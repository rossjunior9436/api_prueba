﻿using Prueba.DDD.Domain.ValueObjects;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prueba.DDD.Domain.Entities
{

    public class Guest_reservation
    {
        [Key]
        public int Id { get; set; }
        public IdObject Id_reservation { get; set; }
        //public virtual Reservation Reservation { get; set; }
        public IdObject Id_guest { get; set; }
        //public virtual Guest Guest { get; set; }

        public void SetId_reservation(IdObject id_reservation)
        {
            this.Id_reservation = id_reservation;
        }
        public void SetId_guest(IdObject id_guest)
        {
            this.Id_guest = id_guest;
        }

    }
}
