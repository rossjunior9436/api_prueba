﻿using Prueba.DDD.Domain.ValueObjects;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prueba.DDD.Domain.Entities
{
    public class Reservation
    {
        [Key]
        public int Id { get; set; }
        public IdObject Id_hotel { get; set; }
        public virtual Hotel Hotel { get; set; }
        public IdObject Id_room { get; set; }
        public virtual Room Room { get; set; }
        public CheckDate CheckIn { get; set; }
        public CheckDate CheckOut { get; set; }
        public Price Total_price { get; set; }
        public Capacity Total_guest { get; set; }

        [StringLength(1)]
        public State State { get; set; }
        public void SetId_hotel(IdObject id_hotel)
        {
            this.Id_hotel = id_hotel;
        }
        public void SetId_room(IdObject id_room)
        {
            this.Id_room = id_room;
        }
        public void SetCheckIn(CheckDate checkIn)
        {
            this.CheckIn = checkIn;
        }
        public void SetCheckOut(CheckDate checkOut)
        {
            this.CheckOut = checkOut;
        }
        public void SetTotal_price(Price price)
        {
            this.Total_price = price;
        }
        public void SetTotal_guest(Capacity capacity)
        {
            this.Total_guest = capacity;
        }
        public void SetState(State state)
        {
            this.State = state;
        }



    }
}
