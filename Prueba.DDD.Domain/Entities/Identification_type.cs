﻿using Prueba.DDD.Domain.ValueObjects;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prueba.DDD.Domain.Entities
{
    public class Identification_type
    {
        [Key]
        public int Id { get; set; }
        public Name Name { get; private set; }
        public State State { get; private set; }
        public void SetName(Name name)
        {
            this.Name = name;
        }
        public void SetState(State state)
        {
            this.State = state;
        }


    }
}
