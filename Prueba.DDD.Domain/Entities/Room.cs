﻿using Prueba.DDD.Domain.ValueObjects;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prueba.DDD.Domain.Entities
{

    public class Room
    {
        [Key]
        public int Id { get; set; }
        public Name Name { get;  set; }
        public IdObject Id_hotel { get;  set; }
        public Price Price { get;  set; }
        public virtual Hotel Hotel { get; set; }
        public Capacity Capacity { get;  set; }

        [StringLength(1)]
        public State State { get;  set; }
        public void SetName(Name name)
        {
            this.Name = name;
        }
        public void SetId_hotel(IdObject id_hotel)
        {
            this.Id_hotel = id_hotel;
        }
        public void SetPrice(Price price)
        {
            this.Price = price;
        }
        public void SetCapacity(Capacity capacity)
        {
            this.Capacity = capacity;
        }
        public void SetState(State state)
        {
            this.State = state;
        }


    }
}
