﻿using Prueba.DDD.Domain.Entities;
using Prueba.DDD.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prueba.DDD.Infraestructure.Repository
{
    public class IdentificationRepository : IidentificationTypeRepository
    {
        DataBaseContext _context;
        public IdentificationRepository(DataBaseContext context)
        {
            _context = context;
        }

        public async Task<Identification_type> GetIdentificationTypeById(int id)
        {
            var identificationType = await _context.Identification_types.FindAsync(id);
            

            return identificationType;


        }
    }
}
