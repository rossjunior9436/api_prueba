﻿using Microsoft.EntityFrameworkCore;
using Prueba.DDD.Domain.Entities;
using Prueba.DDD.Domain.Repositories;
using Prueba.DDD.Domain.ValueObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prueba.DDD.Infraestructure.Repository
{
    public class HotelRepository: IHotelRepository
    {
        DataBaseContext _context;
        public HotelRepository(DataBaseContext context)
        {
            _context = context;
        }
        public async Task AddHotel(Hotel hotel)
        {
            await _context.AddAsync(hotel);
            await _context.SaveChangesAsync();
        }

        public async Task UpdateHotel(Hotel hotel)
        {
            _context.Update(hotel);
            await _context.SaveChangesAsync();
        }
        public async Task<Hotel> GetHotelById(int id)
        {
            var hotelWithCity = await _context.Hotels
            .Join(_context.Cities,
                  hotel => hotel.Id_city.Value,
                  city => city.Id,
                  (hotel, city) => new { Hotel = hotel, City = city })
            .Where(hotelCity => hotelCity.Hotel.Id == id)
            .Select(hotelCity => new Hotel
            {
                Id = hotelCity.Hotel.Id,
                Name = hotelCity.Hotel.Name,
                Id_city = hotelCity.Hotel.Id_city, 
                City = hotelCity.City,  
                State = hotelCity.Hotel.State
            })
            .AsNoTracking()
            .FirstOrDefaultAsync();

            return hotelWithCity;


        }

    }
}
