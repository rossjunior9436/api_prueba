﻿using Microsoft.EntityFrameworkCore;
using Prueba.DDD.Domain.Entities;
using Prueba.DDD.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prueba.DDD.Infraestructure.Repository
{

    public class RoomRepository : IRoomRepository
    {
        DataBaseContext _context;
        public RoomRepository(DataBaseContext context)
        {
            _context = context;
        }
        public async Task AddRoom(Room room)
        {
            await _context.AddAsync(room);
            await _context.SaveChangesAsync();
        }

        public async Task UpdateRoom(Room hotel)
        {
            _context.Update(hotel);
            await _context.SaveChangesAsync();
        }
        public async Task<Room> GetRoomById(int id)
        {
            var hotelWithCity = await _context.Rooms
            .Join(_context.Hotels,
                  room => room.Id_hotel.Value,
                  hotel => hotel.Id,
                  (room, hotel) => new { Room= room, Hotel = hotel })
            .Where(roomHotel => roomHotel.Room.Id == id)
            .Select(roomHotel => new Room
            {
                Id = roomHotel.Room.Id,
                Name = roomHotel.Room.Name,
                Price = roomHotel.Room.Price,
                Capacity = roomHotel.Room.Capacity,
                Id_hotel = roomHotel.Room.Id_hotel,
                Hotel = roomHotel.Hotel,
                State = roomHotel.Room.State
            })
            .AsNoTracking()
            .FirstOrDefaultAsync();

            return hotelWithCity;


        }

        public async Task<List<Room>> GetAvailableRoom(DateTime checking, DateTime CheckOut, int totalGuest, int id_city)
        {
            var availableRoom = await (
                    from room in _context.Rooms
                    join hotel in _context.Hotels on room.Id_hotel.Value equals hotel.Id
                    join reservation in _context.Reservations
                        on new { RoomId = room.Id, CityId = hotel.Id_city.Value }
                        equals new { RoomId = reservation.Id_room.Value, CityId = reservation.Id_hotel.Value }
                        into reservations
                    from r in reservations.DefaultIfEmpty()
                    where hotel.Id_city.Value == id_city && room.State.Value == 'A' && hotel.State.Value == 'A' &&
                          (r == null || checking >= r.CheckOut.Value || CheckOut <= r.CheckIn.Value)
                select new Room
                {
                    Id = room.Id,
                    Name = room.Name,
                    Price = room.Price,
                    Capacity = room.Capacity,
                    Id_hotel = room.Id_hotel,
                    Hotel = hotel,
                    State = room.State
                }
            )
            .AsNoTracking()
            .ToListAsync();


            return availableRoom;


        }
    }
}
