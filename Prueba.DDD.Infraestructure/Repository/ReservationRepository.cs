﻿using Microsoft.EntityFrameworkCore;
using Prueba.DDD.Domain.Entities;
using Prueba.DDD.Domain.Repositories;
using Prueba.DDD.Domain.ValueObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prueba.DDD.Infraestructure.Repository
{

    public class ReservationRepository : IReservationRepository
    {
        DataBaseContext _context;
        public ReservationRepository(DataBaseContext context)
        {
            _context = context;
        }
        public async Task AddReservation(Reservation reservation, List<Guest> guets)
        {
            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    await _context.AddAsync(reservation);
                    await _context.SaveChangesAsync();
                    int geneneratedIdReservation = reservation.Id;

                    foreach (var itemGuest in guets)
                    {
                        await _context.AddAsync(itemGuest);
                        await _context.SaveChangesAsync();
                        int geneneratedIdGuest = itemGuest.Id;
                        Guest_reservation guest_Reservation = new Guest_reservation();
                        guest_Reservation.Id = 0;
                        guest_Reservation.SetId_reservation(IdObject.Create(geneneratedIdReservation));
                        guest_Reservation.SetId_guest(IdObject.Create(geneneratedIdGuest));
                        await _context.AddAsync(guest_Reservation);
                        await _context.SaveChangesAsync();
                    }

                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();

                }
            }
            
        }

        public async Task<List<Reservation>> GetReservationByRangeDate(DateTime initialDate, DateTime finalDate)
        {

            var availableRoom = await (
                   from reservation in _context.Reservations
                   join hotel in _context.Hotels on reservation.Id_hotel.Value equals hotel.Id
                   join room in _context.Rooms on reservation.Id_room.Value equals room.Id
                   where (reservation.CheckIn.Value >= initialDate && reservation.CheckIn.Value <= finalDate) ||
                         (reservation.CheckOut.Value >= initialDate && reservation.CheckOut.Value <= finalDate) ||
                         (reservation.CheckIn.Value <= initialDate && reservation.CheckOut.Value >= finalDate)
                   select new Reservation
                   {
                       Id = reservation.Id,
                       Id_hotel = reservation.Id_hotel,
                       CheckIn = reservation.CheckIn,
                       CheckOut = reservation.CheckOut,
                       Total_guest = reservation.Total_guest,
                       Total_price = reservation.Total_price,
                       Hotel = hotel,
                       Room = room,
                       State = reservation.State
                   }
           )
           .AsNoTracking()
           .ToListAsync();


            return availableRoom;


        }
    }
}
