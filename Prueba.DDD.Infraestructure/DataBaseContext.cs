﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Prueba.DDD.Domain.Entities;
using Prueba.DDD.Domain.ValueObjects;
using System;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Xml.Linq;

namespace Prueba.DDD.Infraestructure
{
    public class DataBaseContext:DbContext
    {
        public DataBaseContext(DbContextOptions<DataBaseContext> options):base(options) { }

        public DbSet<Hotel> Hotels { get; set; }
        public DbSet<City> Cities { get; set; }
        public DbSet<Room> Rooms { get; set; }
        public DbSet<Identification_type> Identification_types { get; set; }
        public DbSet<Reservation> Reservations { get; set; }
        public DbSet<Guest> Guests { get; set; }
        public DbSet<Guest_reservation> Guest_reservations { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Hotel>(o =>
            {
                o.HasKey(x => x.Id);
                o.OwnsOne(x => x.Name).Property(x => x.Value).HasColumnName("Name");
                o.OwnsOne(x => x.Id_city).Property(x => x.Value).HasColumnName("Id_city");
                o.OwnsOne(x => x.State).Property(x => x.Value).HasColumnName("State");
            });


            modelBuilder.Entity<Hotel>()
            .HasOne(x => x.City)
            .WithOne()
            .HasForeignKey<City>(x => x.Id);

            /*modelBuilder.Entity<Hotel>()
            .HasOne(x => x.City)
            .WithOne()
            .HasForeignKey<Hotel>(x => x.Id_city);*/

            /*modelBuilder.Entity<Hotel>().OwnsOne(o => o.Name, conf =>
            {
                conf.Property(x => x.Value).HasColumnName("Name");
            });

           modelBuilder.Entity<Hotel>().OwnsOne(o => o.Id_city, conf =>
            {
                conf.Property(x => x.Value).HasColumnName("Id_city");
            });

            modelBuilder.Entity<Hotel>().OwnsOne(o => o.State, conf =>
            {
                conf.Property(x => x.Value).HasColumnName("State");
            })*/

            modelBuilder.Entity<City>(o =>
            {
                o.HasKey(x => x.Id);
                o.OwnsOne(x => x.Name).Property(x => x.Value).HasColumnName("Name");
                o.OwnsOne(x => x.State).Property(x => x.Value).HasColumnName("State");
            });

            modelBuilder.Entity<Room>(o =>
            {
                o.HasKey(x => x.Id);
                o.OwnsOne(x => x.Name).Property(x => x.Value).HasColumnName("Name");
                o.OwnsOne(x => x.Id_hotel).Property(x => x.Value).HasColumnName("Id_hotel");
                o.OwnsOne(x => x.Price).Property(x => x.Value).HasColumnName("Price");
                o.OwnsOne(x => x.Capacity).Property(x => x.Value).HasColumnName("Capacity");
                o.OwnsOne(x => x.State).Property(x => x.Value).HasColumnName("State");
               
            });

            modelBuilder.Entity<Room>()
            .HasOne(x => x.Hotel)
            .WithOne()
            .HasForeignKey<Hotel>(x => x.Id);

            modelBuilder.Entity<Identification_type>(o =>
            {
                o.HasKey(x => x.Id);
                o.OwnsOne(x => x.Name).Property(x => x.Value).HasColumnName("Name");
                o.OwnsOne(x => x.State).Property(x => x.Value).HasColumnName("State");
            });

            modelBuilder.Entity<Guest>(o =>
            {
                o.HasKey(x => x.Id);
                o.OwnsOne(x => x.NameAndLastName).Property(x => x.Value).HasColumnName("Name_lastname");
                o.OwnsOne(x => x.BirthDate).Property(x => x.Value).HasColumnName("BirthDate");
                o.OwnsOne(x => x.Gender).Property(x => x.Value).HasColumnName("Gender");
                o.OwnsOne(x => x.Id_Type).Property(x => x.Value).HasColumnName("Id_type");
                o.OwnsOne(x => x.NumberId).Property(x => x.Value).HasColumnName("Number_Id");
                o.OwnsOne(x => x.EmailAddress).Property(x => x.Value).HasColumnName("Email_address");
                o.OwnsOne(x => x.PhoneNumber).Property(x => x.Value).HasColumnName("Phone_Number");
                o.OwnsOne(x => x.ContactEmergencyNameAndLastName).Property(x => x.Value).HasColumnName("ContactEmergency_Name_lastname");
                o.OwnsOne(x => x.ContactEmergencyPhoneNumber).Property(x => x.Value).HasColumnName("ContactEmergency_Phone_Number");
                o.OwnsOne(x => x.State).Property(x => x.Value).HasColumnName("State");

            });

            modelBuilder.Entity<Guest>()
            .HasOne(x => x.Identification_Type)
            .WithOne()
            .HasForeignKey<Identification_type>(x => x.Id);

            modelBuilder.Entity<Reservation>(o =>
            {
                o.HasKey(x => x.Id);
                o.OwnsOne(x => x.Id_hotel).Property(x => x.Value).HasColumnName("Id_hotel");
                o.OwnsOne(x => x.Id_room).Property(x => x.Value).HasColumnName("Id_room");
                o.OwnsOne(x => x.CheckIn).Property(x => x.Value).HasColumnName("Checkin");
                o.OwnsOne(x => x.CheckOut).Property(x => x.Value).HasColumnName("Checkout");
                o.OwnsOne(x => x.Total_price).Property(x => x.Value).HasColumnName("Total_price");
                o.OwnsOne(x => x.Total_guest).Property(x => x.Value).HasColumnName("total_guest");
                o.OwnsOne(x => x.State).Property(x => x.Value).HasColumnName("State");

            });

            modelBuilder.Entity<Reservation>()
            .HasOne(x => x.Hotel)
            .WithOne()
            .HasForeignKey<Hotel>(x => x.Id);

            modelBuilder.Entity<Reservation>()
            .HasOne(x => x.Room)
            .WithOne()
            .HasForeignKey<Room>(x => x.Id);

            modelBuilder.Entity<Guest_reservation>(o =>
            {
                o.HasKey(x => x.Id);
                o.OwnsOne(x => x.Id_reservation).Property(x => x.Value).HasColumnName("Id_reservation");
                o.OwnsOne(x => x.Id_guest).Property(x => x.Value).HasColumnName("Id_guest");

            });
            
            /*modelBuilder.Entity<Guest_reservation>()
            .HasOne(x => x.Reservation)
            .WithOne()
            .HasForeignKey<Reservation>(x => x.Id);

            modelBuilder.Entity<Guest_reservation>()
            .HasOne(x => x.Guest)
            .WithOne()
            .HasForeignKey<Guest>(x => x.Id);*/



            base.OnModelCreating(modelBuilder);
        }
    }
}
